﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;
using System.Timers;
using System.IO;
using Newtonsoft.Json;
using ACC_hotlap.DataJson;


namespace ACC_hotlap
{
    class App
    {
        string logFolder;
        string databaseFile;
        string webPath;
        string configFolder;
        bool debug = false;
        int port = 5678;

        private Timer _refreshServerInfoTimer;

        SimpleHTTPServer myServer;
        //FilterEventHandler filterHandler;

        string targetDirectoryPath;
        List<String> Files = new List<string>();

        Hotlap hotlaps = new Hotlap();

        public App()
        {

            string[] lines = System.IO.File.ReadAllLines("config.ini");

            foreach (string line in lines)
            {
                if (line.Contains("log_folder"))
                    logFolder = line.Substring(11);
                if (line.Contains("database_file"))
                    databaseFile = line.Substring(14);
                if (line.Contains("web_folder"))
                    webPath = line.Substring(11);
                if (line.Contains("port"))
                    port = Int32.Parse(line.Substring(5));
                if (line.Contains("cfg_folder"))
                    configFolder = line.Substring(11);
                if (line.Contains("debug"))
                    debug = bool.Parse(line.Substring(6));
            }

            Configuration.DatabaseFolder = databaseFile;
            Configuration.LogFolder = logFolder;
            Configuration.WebFolder = webPath;
            Configuration.EventFile = configFolder;

            Console.WriteLine(logFolder);
            Console.WriteLine(databaseFile);
            Console.WriteLine(webPath);


            Console.WriteLine(File.Exists(databaseFile) ? "File exists." : "File does not exist.");
            //"ACC_Database.sqlite"
            bool DatabaseExistAtStart = File.Exists(databaseFile);
            if (!DatabaseExistAtStart)
                DatabaseHandler.createNewDatabase();
            DatabaseHandler.connectToDatabase(databaseFile);
            if (!DatabaseExistAtStart)
                DatabaseHandler.createTables();

            //json log folder
            FileSystemWatcher watcher = new FileSystemWatcher();
            watcher.Path = logFolder;

            watcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite
               | NotifyFilters.FileName | NotifyFilters.DirectoryName;

            watcher.Filter = "*.json";

            watcher.Created += new FileSystemEventHandler(OnChanged);

            watcher.EnableRaisingEvents = true;

            //cfg log folder
            FileSystemWatcher cfgWatcher = new FileSystemWatcher();
            cfgWatcher.Path = configFolder;

            cfgWatcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite
               | NotifyFilters.FileName | NotifyFilters.DirectoryName;

            cfgWatcher.Filter = "*.json";

            cfgWatcher.Created += new FileSystemEventHandler(OnChangedCfg);

            cfgWatcher.EnableRaisingEvents = true;

            //this._refreshServerInfoTimer = new Timer();
            //this._refreshServerInfoTimer.Interval = 600000;
            //this._refreshServerInfoTimer.Elapsed += new ElapsedEventHandler(this.TimeElapsed);
            //this._refreshServerInfoTimer.Enabled = true;
        }

        public void parseLogsAndDoHotlap()
        {
            getFilesFromFolder(logFolder);

            //hotlaps.setHotlapFromList(DatabaseHandler.GetLaptimesForCurrentServer(gameSetting[0]));
            //hotlaps.Shrink();
            //hotlaps.calculateGaps();
            /*foreach (HotlapEntity player in hotlaps.getHotlaps())
            {
                Console.WriteLine(player.Name + "  " + player.Car + "  " + player.getLaptimeReadable() + "  " + player.getGapReadable() + "  " + player.getDateReadableFormat().ToString());
            }*/

            ////string createIndexText = HtmlFormatter.getTableInHtmlFormat(hotlaps, DatabaseHandler.getTrackByLayoutID(gameSetting[0].GameSetting.TrackLayouts[0].LayoutId)) + Environment.NewLine;
            //string createIndexText = HtmlFormatter.createIndexHtml(hotlaps, DatabaseHandler.getTrackByLayoutID(gameSetting[0].GameSetting.TrackLayouts[0].LayoutId)) + Environment.NewLine;
            //File.WriteAllText(webPath + "\\index.html", createIndexText);
            ////if (!File.Exists(tablePath))
           // {
           //     // Create a file to write to.
            //    string createText = HtmlFormatter.getTableInHtmlFormat(hotlaps, DatabaseHandler.getTrackByLayoutID(gameSetting[0].GameSetting.TrackLayouts[0].LayoutId)) + Environment.NewLine;
             //   File.WriteAllText(webPath + "\\table.html", createText);
            //}
        }

        //private void TimeElapsed(Object sender, ElapsedEventArgs eventArgs)
        //{
        //    getActualServerInfo();
        //    Console.WriteLine("timer ok");
        //}

        private async void OnChanged(object source, FileSystemEventArgs e)
        {
            // Specify what is done when a file is changed, created, or deleted.
            System.Threading.Thread.Sleep(2000);
            
            parseLogsAndDoHotlap();
            //Console.WriteLine("File: " + e.FullPath + " " + e.ChangeType);
        }

        private static void OnChangedCfg(object source, FileSystemEventArgs e)
        {
            //  Show that a file has been created, changed, or deleted.
            WatcherChangeTypes wct = e.ChangeType;
            Console.WriteLine("File {0} {1}", e.FullPath, wct.ToString());
            if (e.Name.Contains("event"))
            {
                System.Threading.Thread.Sleep(2000);
                parseNewConfigFile(e.FullPath, e.Name);
            }
        }

        public static void parseNewConfigFile(string fullPath, string name)
        {
            string text = System.IO.File.ReadAllText(fullPath);

            Event eventInfo = JsonConvert.DeserializeObject<Event>(text);
            string track = eventInfo.track;
            DatabaseHandler.addTrackWithDateIfNotExists(track, getDateFromString(name));
        }

        public static long getDateFromString(string fileName)
        {
            //Console.WriteLine("ez kell");
            string date = fileName.Substring(fileName.IndexOf('_') + 1, fileName.IndexOf('.') - fileName.IndexOf('_') - 1);
            //Console.WriteLine(date);
            int year = Int32.Parse(date.Substring(0, 4));
            int month = Int32.Parse(date.Substring(4, 2));
            int day = Int32.Parse(date.Substring(6));
            //Console.WriteLine("date {0} {1} {2}", year, month, day);
            return ConvertToUnixTime(new DateTime(year, month, day));
        }

        public void getFilesFromFolder(string path)
        {
            if (Directory.Exists(path))
            {
                // This path is a directory
                ProcessDirectory(path);
            }
            else
            {
                Console.WriteLine("{0} is not a valid file or directory.", path);
            }
        }

        public void ProcessDirectory(string targetDirectory)
        {
            // Process the list of files found in the directory.
            string[] fileEntries = Directory.GetFiles(targetDirectory);
            foreach (string fileName in fileEntries)
            {
                if(debug)
                    Console.WriteLine(fileName + " : " + Path.GetFileName(fileName));
                //Console.WriteLine(DatabaseHandler.isFileExist(Path.GetFileName(fileName)));
                //TODO: don't parse if exists in hotlap
                if (!DatabaseHandler.isFileExist(Path.GetFileName(fileName)))
                    parseFiles(fileName);
                if (debug)
                    Console.WriteLine(Path.GetFileName(fileName));
                DatabaseHandler.addFile(Path.GetFileName(fileName));
            }

            // Recurse into subdirectories of this directory.
            string[] subdirectoryEntries = Directory.GetDirectories(targetDirectory);
            foreach (string subdirectory in subdirectoryEntries)
                ProcessDirectory(subdirectory);
        }

        public void parseFiles(string fileName)
        {
            if (debug)
                Console.WriteLine("parseFiles: " + fileName);
            string text = System.IO.File.ReadAllText(fileName, encoding: Encoding.Unicode);
            DateTime creation = File.GetCreationTime(fileName);
            SessionInfo sessionInfo = JsonConvert.DeserializeObject<SessionInfo>(text);
            parseHotlaps(sessionInfo, creation);
        }

        private void parseHotlaps(SessionInfo info, DateTime creation)
        {
            if (debug)
                Console.WriteLine("CORY: " + info.trackName);

            //  GET SERVER INFO//
            //Console.WriteLine("parseFiles: " + fileName);
            //string text = System.IO.File.ReadAllText(Configuration.EventFile);

            //Event eventInfo = JsonConvert.DeserializeObject<Event>(text);
            //string track = eventInfo.track;
            //int trackID = DatabaseHandler.getTrackID(track);
            int trackID = DatabaseHandler.getTrackByDate(creation);
            if (trackID > 0)
            {
                //Int32 unixTimestamp = (Int32)creation.TotalSeconds;
                foreach (LeaderBoardLine line in info.sessionResult.leaderBoardLines)
                {
                    DatabaseHandler.addPlayerIfNotExists(line.currentDriver);
                    if (debug)
                    {
                        Console.WriteLine("DATE: " + creation.ToShortDateString());
                        Console.WriteLine("DATE: " + creation.ToLongDateString());
                        Console.WriteLine("DATE: " + creation.ToShortTimeString());
                        Console.WriteLine("DATE: " + creation.ToLongTimeString());
                        Console.WriteLine("DATE: " + creation.ToString());
                    }
                    if (line.timing.BestLap != 2147483647)
                    {
                        DatabaseHandler.addHotlap(line.currentDriver, line.car, line.timing, trackID, ConvertToUnixTime(creation));
                    }
                    //Console.WriteLine("CORY: " + line.car.carModel);
                    //Console.WriteLine(line.currentDriver.firstName);
                    //Console.WriteLine(line.currentDriver.lastName);
                    //Console.WriteLine(line.timing.BestLap);
                }
                hotlaps.setHotlapFromList(DatabaseHandler.GetLaptimesForTrack(DatabaseHandler.getTrackByID(trackID)));
                hotlaps.Shrink();
                hotlaps.calculateGaps();

                string createIndexText = HTMLCreator.createIndexHtml(hotlaps, DatabaseHandler.getTrackByID(trackID));
                File.WriteAllText(webPath + "\\index.html", createIndexText);
                //if (!File.Exists(tablePath))
                {
                    // Create a file to write to.
                    //string createText = HTMLCreator.getTableInHtmlFormat(hotlaps, DatabaseHandler.getTrackByLayoutID(gameSetting[0].GameSetting.TrackLayouts[0].LayoutId)) + Environment.NewLine;
                    //File.WriteAllText(webPath + "\\table.html", createText);
                }
            }

        }

        public void generateHtml()
        {
            int trackID = DatabaseHandler.getLastTrack();

            hotlaps.setHotlapFromList(DatabaseHandler.GetLaptimesForTrack(DatabaseHandler.getTrackByID(trackID)));
            hotlaps.Shrink();
            hotlaps.calculateGaps();
            string createIndexText = HTMLCreator.createIndexHtml(hotlaps, DatabaseHandler.getTrackByID(trackID));
            File.WriteAllText(webPath + "\\index.html", createIndexText);
        }

        public static long ConvertToUnixTime(DateTime datetime)
        {
            DateTime sTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

            return (long)(datetime - sTime).TotalSeconds;
        }


        public void startServer()
        {

            string myFolder = webPath;

            //create server with auto assigned port
            myServer = new SimpleHTTPServer(myFolder, port);

            Console.WriteLine("Server is running on this port: " + myServer.Port.ToString());
        }

        public void stopServer()
        {
            myServer.Stop();
        }
    }
}
