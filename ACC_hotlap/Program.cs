﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ACC_hotlap.DataJson;

namespace ACC_hotlap
{
    class Program
    {
        static void Main(string[] args)
        {
            bool stopEverything = false;
            string input;
            //Console.WriteLine("getServerInfoOffline");
            //string path = @"C:\Users\ekorban\Documents\ACC_hotlap\events\190610_173811_0.json";
            //string text = System.IO.File.ReadAllText(path, encoding: Encoding.Unicode);
            //Console.WriteLine(text);


            //SessionResult session = JsonConvert.DeserializeObject<SessionResult>(text);
            //Console.ReadLine();

            App app = new App();
            app.startServer();
            //app.parseLogsAndDoHotlap();
            while (!stopEverything)
            {
                input = Console.ReadLine();
                if (input.Contains("exit"))
                {
                    stopEverything = true;
                    app.stopServer();
                }
                if (input.Contains("html"))
                {
                    app.generateHtml();
                }
            }
        }
    }
}
