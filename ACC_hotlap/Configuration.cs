﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACC_hotlap
{
    static class Configuration
    {
        static string webFolder;
        static string logFolder;
        static string databaseFile;
        static string eventFile;
        static bool debug;

        public static string WebFolder { get => webFolder; set => webFolder = value; }
        public static string LogFolder { get => logFolder; set => logFolder = value; }
        public static string DatabaseFolder { get => databaseFile; set => databaseFile = value; }
        public static string EventFile { get => eventFile; set => eventFile = value; }
        public static bool Debug { get => debug; set => debug = value; }
    }
}
