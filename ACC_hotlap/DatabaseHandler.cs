﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using ACC_hotlap.DataJson;

namespace ACC_hotlap
{
    public static class DatabaseHandler
    {
        static SQLiteConnection m_dbConnection;

        public static void createNewDatabase()
        {
            SQLiteConnection.CreateFile("ACC_Database.sqlite");
        }

        public static void connectToDatabase(string path)
        {
            m_dbConnection = new SQLiteConnection("Data Source=" + path + ";Version=3;");
            m_dbConnection.Open();
        }

        public static void createTables()
        {
            List<string> tableCreateQueries = new List<string>();
            tableCreateQueries.Add("CREATE TABLE IF NOT EXISTS CAR (ID INTEGER PRIMARY KEY, CAR NVARCHAR(50) NULL)");
            tableCreateQueries.Add("CREATE TABLE IF NOT EXISTS TRACK (ID INTEGER PRIMARY KEY, TRACK_NAME NVARCHAR(50) NULL)");
            tableCreateQueries.Add("CREATE TABLE IF NOT EXISTS DRIVER (ID INTEGER PRIMARY KEY AUTOINCREMENT, FIRST_NAME NVARCHAR(50) NULL, LAST_NAME NVARCHAR(50) NULL, SHORT_NAME NVARCHAR(50) NULL, PLAYER_ID INTEGER)");
            //tableCreateQueries.Add("CREATE TABLE IF NOT EXISTS LAPS (ID INTEGER PRIMARY KEY AUTOINCREMENT, CAR_ID NUM, TRACK_ID NUM, PLAYER_ID NUM, LAPTIME NUM, DATE NUM)");
            tableCreateQueries.Add("CREATE TABLE IF NOT EXISTS LAPS (ID INTEGER PRIMARY KEY AUTOINCREMENT, CAR_ID NUM, TRACK_ID NUM, PLAYER_ID NUM, LAPTIME NUM, DATE NUM, S1 NUM, S2 NUM, S3 NUM)");
            tableCreateQueries.Add("CREATE TABLE IF NOT EXISTS FILES (ID INTEGER PRIMARY KEY AUTOINCREMENT, FILE_NAME NVARCHAR(50) NULL)");
            tableCreateQueries.Add("CREATE TABLE IF NOT EXISTS TRACK_DATE (ID INTEGER PRIMARY KEY, TRACK_ID NUM, DATE NUM)");

            List<string> tableConstraints = new List<string>();
            tableConstraints.Add("ALTER TABLE LAPS ADD CONSTRAINT FK_CAR FOREIGN KEY (CAR_ID) REFERENCES CAR(ID)");
            tableConstraints.Add("ALTER TABLE LAPS ADD CONSTRAINT FK_TRACK FOREIGN KEY (TRACK_ID) REFERENCES TRACK(ID)");
            //tableConstraints.Add("ALTER TABLE LAPS ADD CONSTRAINT FK_PLAYER FOREIGN KEY (PLAYER_ID) REFERENCES PLAYER(ID)");

            foreach (string sql in tableCreateQueries)
            {
                SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
                command.ExecuteNonQuery();
            }

            InitializeCarsAndTracks();
            /*
            foreach (string sql in tableConstraints)
            {
                SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
                command.ExecuteNonQuery();
            }*/

            //string sql = "CREATE TABLE IF NOT EXISTS CAR (ID INTEGER PRIMARY KEY, CAR NVARCHAR(50) NULL)";
            //SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            //command.ExecuteNonQuery();


        }

        public static void InitializeCarsAndTracks()
        {
            List<string> cars = new List<string>()
            {
                "Porsche 991 GT3", "Mercedes AMG GT3", "Ferrari 488 GT3", "Audi R8 LMS", "Lamborghini Huracan GT3",
                "Mclaren 650s GT3", "Nissan GT R Nismo GT3 2018", "BMW M6 GT3", "Bentley Continental GT3 2018",
                "Porsche 991.2 GT3 Cup", "Nissan GT-R Nismo GT3 2017", "Bentley Continental GT3 2016",
                "Aston Martin Vantage V12 GT3", "Lamborghini Gallardo R-EX", "Jaguar G3", "Lexus RC F GT3",
                "Lamborghini Huracan Evo (2019)", "Honda NSX GT3", "Lamborghini Huracan SuperTrofeo",
                "Audi R8 LMS Evo (2019)", "AMR V8 Vantage (2019)", "Honda NSX Evo (2019)",
                "McLaren 720S GT3 (Special)", "Porsche 911 II GT3 R (2019)"
            };

            List<string> tracks = new List<string>()
            {
                "monza", "zolder", "brands_hatch", "silverstone", "paul_ricard",
                "misano", "spa", "nurburgring", "barcelona", "hungaroring",
                "zandvoort", "monza_2019", "zolder_2019", "brands_hatch_2019",
                "silverstone_2019", "paul_ricard_2019", "misano_2019", "spa_2019",
                "nurburgring_2019", "barcelona_2019", "hungaroring_2019", "zandvoort_2019",
                "kyalami_2019", "mount_panorama_2019", "suzuka_2019", "laguna_seca_2019"
            };
            // CAR IDS WILL BE INCREMENTED BY 1 //
            cars.ForEach(insertCar);
            tracks.ForEach(insertTrack);
        }

        public static void insertCar(string s)
        {
            string sqlInsert = "INSERT INTO CAR VALUES(NULL, \"" + s + "\");";
            SQLiteCommand command2 = new SQLiteCommand(sqlInsert, m_dbConnection);
            command2.ExecuteNonQuery();
        }

        public static void insertTrack(string s)
        {
            string sqlInsert = "INSERT INTO TRACK VALUES(NULL, \"" + s + "\");";
            SQLiteCommand command2 = new SQLiteCommand(sqlInsert, m_dbConnection);
            command2.ExecuteNonQuery();
        }

        public static int getTrackID(string s)
        {
            string sql = "SELECT ID from TRACK where TRACK_NAME = \"" + s + "\"";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            SQLiteDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                    return reader.GetInt32(0);
            }
            return 0;
        }

        public static string getTrackByID(int s)
        {
            string sql = "SELECT TRACK_NAME from TRACK where ID = \"" + s + "\"";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            SQLiteDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                    return reader.GetString(0);
            }
            return "";
        }

        public static void addFile(string fileName)
        {
            string sql = "SELECT FILE_NAME from FILES where FILE_NAME = \"" + fileName + "\"";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            SQLiteDataReader reader = command.ExecuteReader();
            if (!reader.HasRows)
            {
                string sqlInsert = "INSERT INTO FILES VALUES(NULL, \"" + fileName + "\");";
                SQLiteCommand command2 = new SQLiteCommand(sqlInsert, m_dbConnection);
                command2.ExecuteNonQuery();
            }
        }

        public static bool isFileExist(string fileName)
        {
            string sql = "SELECT FILE_NAME from FILES where FILE_NAME = \"" + fileName + "\"";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            SQLiteDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                return true;
            }
            return false;
        }

        public static void addTrackWithDateIfNotExists(String track, long date)
        {
            string sql = "SELECT DATE from TRACK_DATE where DATE = \"" + date + "\"" + "AND TRACK_ID = \"" + getTrackID(track) + "\"";
            Console.WriteLine(sql);
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            SQLiteDataReader reader = command.ExecuteReader();
            if (!reader.HasRows)
            {
                //"CREATE TABLE IF NOT EXISTS LAPS (ID INTEGER PRIMARY KEY AUTOINCREMENT, CAR_ID NUM, TRACK_ID NUM, PLAYER_ID NUM, LAPTIME NUM, DATE NUM, S1 NUM, S2 NUM, S3 NUM)"
                string sqlInsert = "INSERT INTO TRACK_DATE VALUES(NULL, \"" + getTrackID(track) + "\", \""  + date + "\");";
                SQLiteCommand command2 = new SQLiteCommand(sqlInsert, m_dbConnection);
                command2.ExecuteNonQuery();
            }
        }

        public static int getTrackByDate(DateTime creation)
        {
            List<Tuple<int, DateTime>> events = new List<Tuple<int, DateTime>>();
            int lastDate = -1;
               string sql = "SELECT DATE, TRACK_ID from TRACK_DATE order by DATE;";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            SQLiteDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                if (Configuration.Debug)
                    Console.WriteLine("van match");
                while (reader.Read())
                {
                    events.Add(new Tuple<int, DateTime>(reader.GetInt32(1), getDateReadableFormat(reader.GetInt64(0))));
                }
            }
            for(int i =0; i < events.Count;  i++)
            {
                Console.WriteLine("{0}, {1}", events.ElementAt(i).Item1, events.ElementAt(i).Item2);
                if(creation > events.ElementAt(i).Item2)
                {
                    lastDate = i;
                }
            }
            //foreach(Tuple<string, DateTime> element in events){
            //    Console.WriteLine("{0}, {1}", element.Item1, element.Item2);
//            }
            if(lastDate != -1)
                return events.ElementAt(lastDate).Item1;
            return -1;
        }

        public static DateTime getDateReadableFormat(long date)
        {
            System.DateTime dateTime = new System.DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            return dateTime.AddSeconds(date);
        }

        public static bool addPlayerIfNotExists(Driver driver)
        {
            string sql = "SELECT PLAYER_ID from DRIVER WHERE PLAYER_ID = \"" + driver.playerID + "\";";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            SQLiteDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                return false;
                //while (reader.Read())
                //{
                //    nameId = reader["ID"].ToString();
                //}
            }
            else
            {
                string sqlInsert = "INSERT INTO DRIVER VALUES(NULL, \"" + driver.firstName + "\", \"" + driver.lastName + "\", \"" + driver.shortName + "\", \"" + driver.playerID + "\");";
                SQLiteCommand command2 = new SQLiteCommand(sqlInsert, m_dbConnection);
                command2.ExecuteNonQuery();
            }
            return true;
        }

        public static void addHotlap(Driver driver, Car car, Timing timing, int trackID, long date)
        {
            //"CREATE TABLE IF NOT EXISTS LAPS (ID INTEGER PRIMARY KEY AUTOINCREMENT, CAR_ID NUM, TRACK_ID NUM, PLAYER_ID NUM, LAPTIME NUM, DATE NUM, S1 NUM, S2 NUM, S3 NUM)"
            int carType = car.carModel+1;
            string sqlInsert = "INSERT INTO LAPS VALUES(NULL, \"" + carType + "\", \"" + trackID + "\", \"" + driver.playerID + "\", \"" + timing.BestLap + "\", \"" + date + "\", \"" + timing.bestSplits[0] + "\", \"" + timing.bestSplits[1] + "\", \"" + timing.bestSplits[2] + "\");";
            SQLiteCommand command2 = new SQLiteCommand(sqlInsert, m_dbConnection);
            command2.ExecuteNonQuery();
        }


        public static List<HotlapEntity> GetLaptimesForTrack(string track)
        {

            List<HotlapEntity> hotlaps = new List<HotlapEntity>();
            StringBuilder select = new StringBuilder();

            select.Append("SELECT D.LAST_NAME, D.FIRST_NAME, C.CAR, L.LAPTIME, L.S1, L.S2, L.S3, L.DATE ");
            select.Append("FROM LAPS as L, DRIVER as D, CAR as c, TRACK as T ");
            select.Append("where C.ID = L.CAR_ID and D.player_id = L.PLAYER_ID and T.ID = L.TRACK_ID and T.TRACK_NAME = \"" + track + "\"");
            select.Append("order by L.LAPTIME;");
            //Console.WriteLine(select.ToString());
            SQLiteCommand command = new SQLiteCommand(select.ToString(), m_dbConnection);

            SQLiteDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                //Console.WriteLine("van match");
                while (reader.Read())
                {
                    List<long> tmpSector = new List<long>();
                    tmpSector.Add(reader.GetInt64(4));
                    tmpSector.Add(reader.GetInt64(5));
                    tmpSector.Add(reader.GetInt64(6));
                    hotlaps.Add(new HotlapEntity(reader.GetString(0) + " " + reader.GetString(1), reader.GetString(2), reader.GetInt64(3), reader.GetInt64(7), tmpSector));
                    Console.WriteLine("name: " + reader.GetString(0) + " " + reader.GetString(1) + "car: " + reader.GetString(2) + "time: " + reader.GetInt64(3) +  "date: " + reader.GetInt64(7) + "sectors: " + reader.GetInt64(4) + ", " + reader.GetInt64(5) + ", " + reader.GetInt64(6));
                }
            }

            return hotlaps;
        }

        public static int getLastTrack()
        {
            string sql = "SELECT TRACK_ID FROM 'TRACK_DATE' order by DATE desc LIMIT 1;";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            SQLiteDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                    return reader.GetInt32(0);
            }
            return 0;
        }

    }
}
