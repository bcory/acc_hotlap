﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACC_hotlap
{
    class Hotlap
    {
        List<HotlapEntity> hotlaps;

        public Hotlap()
        {
            hotlaps = new List<HotlapEntity>();
        }

        public void addHotlap(HotlapEntity hotlap)
        {
            hotlaps.Add(hotlap);
        }

        public List<HotlapEntity> getHotlaps()
        {
            return hotlaps;
        }

        public void setHotlapFromList(List<HotlapEntity> hotlapList)
        {
            this.hotlaps = hotlapList;
        }

        public void calculateGaps()
        {
            foreach (HotlapEntity lap in hotlaps)
            {
                lap.Gap = lap.Time - hotlaps.First().Time;
            }
        }

        public void Shrink()
        {
            List<HotlapEntity> onlyBest = new List<HotlapEntity>();
            List<string> players = new List<string>();
            foreach (HotlapEntity entry in hotlaps)
            {
                if (!players.Contains(entry.Name))
                {
                    players.Add(entry.Name);
                    onlyBest.Add(entry);
                }
            }
            this.hotlaps = onlyBest;
        }
    }
}
