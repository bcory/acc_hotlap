﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACC_hotlap
{
    class HTMLCreator
    {
        public static string createIndexHtml(Hotlap hotlaps, string track)
        {
            StringBuilder html = new StringBuilder();
            html.Append("<html><head><meta charset = \"UTF-8\">");
            html.Append("<link rel=\"stylesheet\" type=\"text/css\" href=\"my.css\">");
            //html.Append("<table><tr><th>track</th><th>layout</th>");
            //html.Append("<div style=\"width:400px; margin-right:auto; margin-left:auto; border:1px solid #000;\">");

            //html.Append("<div class=\"grid-item\">");
            
            html.Append("<img src=\"logo.png\" alt=\"PSL\">");
            html.Append("<table align=\"center\"><tr><td class=\"bigyo\">");
            
            html.Append(getTableInHtmlFormat(hotlaps, track));
            html.Append("</head></html>");


            return html.ToString();
        }

        public static string getTableInHtmlFormat(Hotlap hotlaps, string track)
        {
            List<string> bestCars = new List<string>();

            double HundredAndSevenPercentTime = 1.0;
            if (hotlaps.getHotlaps().Count>0)
                HundredAndSevenPercentTime = hotlaps.getHotlaps()[0].Time * 1.07;
            StringBuilder htmlTable = new StringBuilder();
            //htmlTable.Append("<html><head><meta charset = \"UTF-8\">");
            
            htmlTable.Append("<h1 align=\"center\">" + track + "</h1>");
            htmlTable.Append("</td></tr></table>");
            htmlTable.Append("<table align=\"center\"><tr><th>Name</th><th>Car</th><th>Laptime</th><th>Gap</th><th>S1</th><th>S2</th><th>S3</th><th>Date</th>");
            foreach (HotlapEntity line in hotlaps.getHotlaps())
            {
                if (bestCars.Contains(line.Car))
                    if (line.Time > HundredAndSevenPercentTime)
                        htmlTable.Append("<tr class=\"hundredandsevenpercent\">");
                    else
                        htmlTable.Append("<tr>");
                else
                {
                    htmlTable.Append("<tr class=\"purple\">");
                    bestCars.Add(line.Car);
                }
                htmlTable.Append(addTableColumn(line.Name, 20));
                htmlTable.Append(addTableColumn(line.Car, 25));
                htmlTable.Append(addTableColumn(line.getLaptimeReadable(), 10, "center"));
                htmlTable.Append(addTableColumn(line.getGapReadable(), 5, "center"));
                //sectors
                htmlTable.Append(addTableColumn(line.getTimeReadable(line.Sectors[0]), 5, "center"));
                htmlTable.Append(addTableColumn(line.getTimeReadable(line.Sectors[1]), 5, "center"));
                htmlTable.Append(addTableColumn(line.getTimeReadable(line.Sectors[2]), 5, "center"));
                Console.WriteLine("HTML: " + line.Date);
                Console.WriteLine(line.getDateReadableFormat().ToShortDateString() + " " + line.getDateReadableFormat().ToShortTimeString());
                htmlTable.Append(addTableColumn(line.getDateReadableFormat().ToShortDateString() + " " + line.getDateReadableFormat().ToShortTimeString(), 20, "center"));
                htmlTable.Append("</tr>");
            }
            //htmlTable.Append("</head></html>");
            //Console.WriteLine(htmlTable.ToString());
            return htmlTable.ToString();
        }

        private static string addTableColumn(string field, int percent)
        {
            return "<td width=\"" + percent + "%\">" + field + "</td>";
        }

        private static string addTableColumn(string field, int percent, string position)
        {
            return "<td align=\"" + position + "\" width=\"" + percent + "%\">" + field + "</td>";
        }

        private static string addOption(string field, long value)
        {
            return "<option value=\"" + value + "\">" + field + "</option>";
        }
    }
}
