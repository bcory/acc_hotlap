﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACC_hotlap.DataJson
{
    public class Car
    {
        public int carId { get; set; }
        public int raceNumber { get; set; }
        public int carModel { get; set; }
        public int cupCategory { get; set; }
        public string teamName { get; set; }
        public string nationality { get; set; }
        public int carGuid { get; set; }
        public int teamGuid { get; set; }
        public List<Driver> drivers { get; set; }

    }
}
