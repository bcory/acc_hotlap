﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACC_hotlap.DataJson
{
    class Event
    {
		public string track { get; set; }
		public int preRaceWaitingTimeSeconds { get; set; }
		public int sessionOverTimeSeconds { get; set; }
        public int ambientTemp { get; set; }
		public double cloudLevel { get; set; }
        public double rain { get; set; }
		public int weatherRandomness  { get; set; }
		public List<serverInfo.Session> sessions { get; set; }
    }
}
