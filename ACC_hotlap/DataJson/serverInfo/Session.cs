﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACC_hotlap.DataJson.serverInfo
{
    class Session
    {
        int hourOfDay { get; set; }
        int dayOfWeekend { get; set; }
        int timeMultiplier { get; set; }
        SessionType sessionType { get; set; }
        int sessionDurationMinutes { get; set; }
    }
}
