﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACC_hotlap.DataJson
{
    class LeaderBoardLine
    {
        public Car car { get; set; }
        public Driver currentDriver { get; set; }
        public int currentDriverIndex { get; set; }
        public Timing timing { get; set; }
        public int missingMandatoryPitstop { get; set; }
        public List<long> driverTotalTimes { get; set; }

    }
}
