﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACC_hotlap.DataJson
{
    public class Timing
    {
        public long lastLap { get; set; }
        public List<long> lastSplits { get; set; }
        public long BestLap { get; set; }
        public List<long> bestSplits { get; set; }
        public long totalTime { get; set; }
        public int lapCount { get; set; }
        public long lastSplitID { get; set; }
    }
}
