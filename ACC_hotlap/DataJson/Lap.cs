﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACC_hotlap.DataJson
{
    class Lap
    {
      public int carId { get; set; }
      public int driverIndex { get; set; }
      public long laptime { get; set; }
      public bool isValidForBest { get; set; }
      public List<long> splits { get; set; }
    }
}
