﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACC_hotlap.DataJson
{
    public class Driver
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string shortName { get; set; }
        public int driverCategory { get; set; }
        public string playerID { get; set; }
    }
}
