﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACC_hotlap.DataJson
{
    class SessionInfo
    {
        public string sessionType { get; set; }
        public string trackName { get; set; }
        public int sessionIndex { get; set; }
        public int raceWeekendIndex { get; set; }
        public string metadata { get; set; }
        public string serverName { get; set; }
        public SessionResult sessionResult { get; set; }
        public List<Lap> laps { get; set; }
    }
}
