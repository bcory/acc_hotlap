﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACC_hotlap
{
    public class HotlapEntity
    {
        public string Name { get; set; }
        public string Car { get; set; }
        public long Time { get; set; }
        public long Gap { get; set; }
        public List<long> Sectors { get; set; }
        public long Date { get; set; }

        public HotlapEntity(string name, string car, long time, long gap, long date, List<long> sectors)
        {
            this.Name = name;
            this.Car = car;
            this.Time = time;
            this.Gap = gap;
            this.Sectors = sectors;
            this.Date = date;
        }

        public HotlapEntity(string name, string car, long time, long date, List<long> sectors)
        {
            this.Name = name;
            this.Car = car;
            this.Time = time;
            this.Sectors = sectors;
            this.Date = date;
            //Console.WriteLine("name: " + name + "car: " + car + "carid: " + CarId + "time: " + time + "date: " + date + "sectors: " + sectors[0] + ", " + sectors[1] + ", " + sectors[2]);
        }

        public DateTime getDateReadableFormat()
        {
            System.DateTime dateTime = new System.DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            return dateTime.AddSeconds(this.Date);
        }

        public string getLaptimeReadable()
        {
            string answer = "";
            TimeSpan t = TimeSpan.FromMilliseconds(Time);
            if (Time < 600000)
            {
                answer = string.Format("{0:D1}:{1:D2}.{2:D3}",
                                    t.Minutes,
                                    t.Seconds,
                                    t.Milliseconds);
            }
            else
            {
                answer = string.Format("{0:D2}:{1:D2}.{2:D3}",
                                    t.Minutes,
                                    t.Seconds,
                                    t.Milliseconds);
            }
            return answer;
        }

        public string getGapReadable()
        {
            string answer = "";
            if (Gap == 0)
            {
                return answer;
            }

            TimeSpan t = TimeSpan.FromMilliseconds(Gap);
            if (Gap < 10000)
            {
                answer = string.Format("{0:D1}.{1:D3}",
                                    t.Seconds,
                                    t.Milliseconds);
            }
            else
            {
                answer = string.Format("{0:D2}.{1:D3}",
                                    t.Seconds,
                                    t.Milliseconds);
            }
            return answer;
        }

        public string getTimeReadable(long time)
        {
            string answer = "";
            if (time == 0)
            {
                return answer;
            }

            TimeSpan t = TimeSpan.FromMilliseconds(time);
            if (time < 10000)
            {
                answer = string.Format("{0:D1}.{1:D3}",
                                    t.Seconds,
                                    t.Milliseconds);
            }
            else
            {
                answer = string.Format("{0:D2}.{1:D3}",
                                    t.Seconds,
                                    t.Milliseconds);
            }
            return answer;
        }
    }
}
